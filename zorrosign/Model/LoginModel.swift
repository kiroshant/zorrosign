//
//  LoginModel.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

@objc protocol LoginDelegate {
    /**
     Executes when the login API call retrieves the result.
     
     - parameter status: true if successfully logged in. false if not.
     - parameter error:  Login error.
     */
    @objc optional func loginCallFinished(_ status: Bool, error: NSError?, userInfo: [String: AnyObject]?)
    
}

class LoginModel:NSObject {
    
    fileprivate let api = ApiClient()
    var Token: String = "String"
    
    var delegate: LoginDelegate?

    
    func login(username: String, password: String) {
        
            api.login(username: username, password: password, success: { (data, code) -> Void in
                
                let jsonData = JSON(data as Any)
                NSLog("userLogin : \(jsonData)")
                
                switch code {
                case 1000:
                    
                    Preferences.setAccessToken(jsonData[AuthUser.JsonKeys.access_token].string ?? nil)
                    
                    print(jsonData[AuthUser.JsonKeys.access_token].string ?? nil)
                    
                    self.delegate?.loginCallFinished!(true, error: nil, userInfo: nil)
                    
                default: break
                
                }
                
                
            }) { (error) -> Void in
                NSLog("Error (Login): \(error.localizedDescription)")
                self.delegate?.loginCallFinished!(false, error: error, userInfo: nil)
            }
        }
    
}
