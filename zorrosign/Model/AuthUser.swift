//
//  AuthUser.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import UIKit

class AuthUser: NSObject, NSCoding {
    
    var accessToken: String
    
    init(accessToken: String){
        self.accessToken = accessToken
    }
       
       
    required init(coder decoder: NSCoder) {
        self.accessToken = decoder.decodeObject(forKey: "accessToken") as? String ?? ""
    }
       
    func encode(with coder: NSCoder) {
        coder.encode(accessToken, forKey: "accessToken")
    }
}

extension AuthUser {
    internal struct JsonKeys {
        static let access_token = "ServiceToken"
    }
}
