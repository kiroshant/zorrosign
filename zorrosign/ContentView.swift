//
//  ContentView.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
