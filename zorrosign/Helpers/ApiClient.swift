//
//  ApiClient.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import Alamofire
import SwiftyJSON

class ApiClient {
    
    enum APICalls {
        case Login
    }

    struct StringKeys{
        static let HEADER_AUTHORIZATION = "Authorization"
        static let HEADER_TOKEN_AUTHENTICATION = "Bearer-Access-Token"
        static let HEADER_CONTENT_TYPE = "Content-Type"
        static let CONTENT_TYPE = "application/json"
        
        static let USERNAME = "Username";
        static let PASSWORD = "Password";
        static let CLIENTID = "ClientId";
        static let CLIENTSECRET = "ClientSecret";
        static let DONOTSENDACTIVATIONMAIL = "DoNotSendActivationMail";
    }

    internal func login(username: String, password: String, success: @escaping (_ data: AnyObject?, _ code: Int) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        
        let url = URL(string: zAPIBaseUrl)
        
        let headers: HTTPHeaders = [:]
        
        let parameters = [
            
            StringKeys.USERNAME:username,
            StringKeys.PASSWORD:password,
            StringKeys.CLIENTID:zClientId,
            StringKeys.CLIENTSECRET:zClientSecret,
            StringKeys.DONOTSENDACTIVATIONMAIL:zDoNotSendActivationMail
            
            ] as [String : Any]
    
        request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .failure:
                // handle errors (including `validate` errors) here

                if let statusCode = response.response?.statusCode {
                    if statusCode == 409 {
                        // handle 409 specific error here, if you want
                    }
                }
            case .success(let value):
                // handle success here
                success(value as AnyObject, 1000)
                print(value)
            }
        }
        
    }

}
