//
//  Preferences.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import UIKit

import UIKit

/**
 *  Wrapper around NSUserDefaults
 */
class Preferences {
    
    fileprivate class func setValue(_ value: AnyObject?, key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    fileprivate class func getValue(_ key: String) -> AnyObject? {
        return UserDefaults.standard.object(forKey: key) as AnyObject?
    }
       
    fileprivate class func removeValue(_ key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
}

extension Preferences {
    
    fileprivate struct PreferencesConstants {
           static let OauthAccessToken = "Oauth_AccessToken"
       }
    
    class func setAccessToken(_ accessToken: String?) {
        Preferences.setValue(accessToken as AnyObject?, key: PreferencesConstants.OauthAccessToken)
    }
}
