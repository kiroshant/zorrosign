//
//  PdfViewController.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import UIKit
import PDFKit

class PdfViewController: UIViewController {
    
    // MARK: - Properties
    var pdfView = PDFView()
    var pdfURL: URL!
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
        view.addSubview(pdfView)
        //print(url as Any)
        /*if let _url = URL(string: url!) {
            UIApplication.shared.open(_url)
        }*/
        
        if let document = PDFDocument(url: pdfURL) {
            pdfView.document = document
        }
        
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        pdfView.frame = view.frame
    }
    
    
    // MARK: - Selectors
    @objc func handleDismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    // MARK: - Helper Functions
    func configureUI() {
        view.backgroundColor = .white
        
        navigationController?.navigationBar.barTintColor = UIColor(red:0.00, green:0.45, blue:0.67, alpha:1.0)
        //navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Settings Web"
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleDismiss))
    }
    
}
