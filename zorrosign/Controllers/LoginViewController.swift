//
//  LoginViewController.swift
//  zorrosign
//
//  Created by Kiroshan Thayaparan on 12/22/19.
//  Copyright © 2019 Kiroshan Thayaparan. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController {
    
    let loginModel = LoginModel()
    
    let containerView =  UIView()
    let usernameTxt = UITextField()
    let passwordTxt =  UITextField()
    let usernameLab = UILabel()
    let passwordLab =  UILabel()
    let button = UIButton()
    var bColor = UIColor(red:0.67, green:0.70, blue:0.73, alpha:1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboardDismissRecognizer()
        print("Please  Password!")
        view.backgroundColor = .white
        setup(view: view)
    }
    
    func setup(view: UIView) {
        
        containerView.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width, height: 240)
        containerView.center = view.center
        view.addSubview(containerView)
        
        usernameLab.frame = CGRect(x: 20, y: 0, width:UIScreen.main.bounds.size.width - 40.0, height: 40)
        usernameLab.text = "Username"
        usernameLab.font = UIFont.systemFont(ofSize: 15)
        usernameLab.textColor = .darkGray
        containerView.addSubview(usernameLab)
        
        usernameTxt.frame = CGRect(x: 20, y: usernameLab.frame.height, width:UIScreen.main.bounds.size.width - 40.0, height: 40)
        usernameTxt.font = UIFont.systemFont(ofSize: 15)
        usernameTxt.textColor = .darkGray
        usernameTxt.textAlignment = .center
        usernameTxt.borderStyle = UITextField.BorderStyle.roundedRect
        usernameTxt.layer.borderWidth = 1.0
        usernameTxt.layer.borderColor = bColor.cgColor
        usernameTxt.autocorrectionType = UITextAutocorrectionType.no
        usernameTxt.keyboardType = UIKeyboardType.default
        usernameTxt.returnKeyType = UIReturnKeyType.done
        usernameTxt.clearButtonMode = UITextField.ViewMode.whileEditing
        usernameTxt.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        usernameTxt.delegate = self
        containerView.addSubview(usernameTxt)
        
        passwordLab.frame = CGRect(x: 20, y: usernameLab.frame.height+usernameTxt.frame.height, width:UIScreen.main.bounds.size.width - 40.0, height: 40)
        passwordLab.text = "Password"
        passwordLab.font = UIFont.systemFont(ofSize: 15)
        passwordLab.textColor = .darkGray
        containerView.addSubview(passwordLab)
        
        passwordTxt.frame = CGRect(x: 20, y: usernameLab.frame.height+usernameTxt.frame.height+passwordLab.frame.height, width:UIScreen.main.bounds.size.width - 40.0, height: 40)
        passwordTxt.font = UIFont.systemFont(ofSize: 15)
        passwordTxt.isSecureTextEntry = true
        passwordTxt.textColor = .darkGray
        passwordTxt.textAlignment = .center
        passwordTxt.borderStyle = UITextField.BorderStyle.roundedRect
        passwordTxt.layer.borderWidth = 1.0
        passwordTxt.layer.borderColor = bColor.cgColor
        passwordTxt.autocorrectionType = UITextAutocorrectionType.no
        passwordTxt.keyboardType = UIKeyboardType.default
        passwordTxt.returnKeyType = UIReturnKeyType.done
        passwordTxt.clearButtonMode = UITextField.ViewMode.whileEditing
        passwordTxt.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        passwordTxt.delegate = self
        containerView.addSubview(passwordTxt)
        
        button.frame = CGRect(x: 20, y: usernameLab.frame.height+usernameTxt.frame.height+passwordLab.frame.height+passwordTxt.frame.height+40, width:UIScreen.main.bounds.size.width - 40.0, height: 40)
        button.backgroundColor = UIColor(red:0.00, green:0.45, blue:0.67, alpha:1.0)
        button.transform = CGAffineTransform.identity
        button.layer.cornerRadius = 5
        button.layer.animationKeys()
        button.setTitle("Download", for: .normal)
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        containerView.addSubview(button)
        
    }
    
    @objc func buttonAction(sender: UIButton!) {
        if usernameTxt.text?.isEmpty ?? true {
             alertDialog(title: "Information", message: "Please enter your Username!")
             print("Please enter your Username!")
         } else if passwordTxt.text?.isEmpty ?? true {
             alertDialog(title: "Information", message: "Please enter your Password!")
             print("Please enter your Password!")
         } else {
            //let controller = PdfViewController()
             //self.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
            
            self.loginModel.login(username: usernameTxt.text!, password: passwordTxt.text!)
         }
    }
    
    
    func alertDialog(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupKeyboardDismissRecognizer(){
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTxt || textField == passwordTxt {
            textField.resignFirstResponder()
            return false
        }
        return true
    }
}
